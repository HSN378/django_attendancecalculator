function Update() {
    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    minutes = checkTime(minutes);
    seconds = checkTime(seconds);

    var timer = document.getElementById("Time");
    timer.innerHTML = hours + ":" + minutes + ":" + seconds;
    var t = setTimeout(Update, 500);

}

function checkTime(i) {
    if (i < 10) { i = "0" + i };  // add zero in front of numbers < 10
    return i;
}